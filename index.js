const axios = require("axios");

async function getStopTimes() {
  try {
    const response = await axios.get("https://data.mobilites-m.fr/api/routers/default/index/stops/SEM:0910/stoptimes", {
      headers: {
        origin: "mon_appli",
      },
    });
    console.log(response.data);
  } catch (error) {
    console.error(error);
  }
}

getStopTimes();
