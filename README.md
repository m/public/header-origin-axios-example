# Node.js Axios Example

This is a simple Node.js project that demonstrates how to use Axios to make a GET request to an API with a custom header.

## Overview

The project contains a script (`index.js`) that makes a GET request to the `data.mobilites-m.fr` API to retrieve stop times for a specific stop using the Axios HTTP client. The request includes a custom header `origin` set to `'mon_appli'`.

## Prerequisites

- Node.js installed on your machine.
- Basic knowledge of terminal and command-line operations.

## Installation

To use this project, follow these steps:

1. Clone the repository or download the project to your local machine.
2. Navigate to the project directory in your terminal.
3. Run `npm install` to install the dependencies (Axios).

## Usage

To run the script, follow these instructions:

1. Navigate to the project's root directory in your terminal.
2. Execute `node index.js` to run the script.

The script will make a request to the API and log the response data to the console. If there is an error, it will log the error message.

## Contributing

If you would like to contribute to this project, please fork the repository and submit a pull request.

## License

This project is open source and available under the [MIT License](LICENSE).

## Contact

If you have any questions or comments about this project, please open an issue in the repository.

Thank you for checking out this Node.js Axios example!
